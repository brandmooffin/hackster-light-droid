﻿using System.Runtime.Serialization;

namespace Light_Droid.Models
{
    public class Schedule
    {
        public int Id { get; set; }

        public string ExecuteTime { get; set; }

        public int Action { get; set; }

        public int Type { get; set; }

        public int EntityType { get; set; }

        public int EntityId { get; set; }

        public string EntityName { get; set; }

        public int UserId { get; set; }

        [IgnoreDataMember]
        public string ActionDescription => Action == 1 ? "On" : "Off";

        [IgnoreDataMember]
        public string ExecuteTimeDescription {
            get {
                switch (Type)
                {
                    case 2:
                        return "Sunset";
                    case 3:
                        return "Sunrise";
                    default:
                        return ExecuteTime;
                };
            }
        }
    }
}
