﻿namespace Light_Droid.Models
{
    public class LifxLight
    {
        public string id { get; set; }
        public string uuid { get; set; }
        public string label { get; set; }
        public bool connected { get; set; }
        public string power { get; set; }
        public int brightness { get; set; }
        public LifxGroup group { get; set; }
        public string last_seen { get; set; }
        public double seconds_since_seen { get; set; }
    }
}
