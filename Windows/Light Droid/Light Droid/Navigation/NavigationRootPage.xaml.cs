﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Light_Droid.Views;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Light_Droid.Navigation
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class NavigationRootPage : Page
    {
        public static NavigationRootPage Current;
        public static Frame RootFrame = null;

        public static SplitView RootSplitView
        {
            get { return Current.rootSplitView; }
        }

        public NavigationRootPage()
        {
            this.InitializeComponent();

            Current = this;
            rootFrame.CacheSize = 2;
            RootFrame = rootFrame;

            //Loaded += NavigationRootPage_Loaded;

            //Use the hardware back button instead of showing the back button in the page
            if (ApiInformation.IsTypePresent("Windows.Phone.UI.Input.HardwareButtons"))
            {
                HardwareButtons.BackPressed += (s, e) =>
                {
                    if (rootFrame.CanGoBack)
                    {
                        rootFrame.GoBack();
                        e.Handled = true;
                    }
                };
            }
        }


        private void AppBar_Closed(object sender, object e)
        {
            //this.navControl.HideSecondLevelNav();
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            this.rootFrame.Navigate(typeof(MainPage));
            rootSplitView.IsPaneOpen = false;
        }

        private void LightsButton_Click(object sender, RoutedEventArgs e)
        {
            this.rootFrame.Navigate(typeof(LightsPage));
            rootSplitView.IsPaneOpen = false;
        }

        private void RoomsButton_Click(object sender, RoutedEventArgs e)
        {
            this.rootFrame.Navigate(typeof(RoomsPage));
            rootSplitView.IsPaneOpen = false;
        }

        private void SchedulesButton_Click(object sender, RoutedEventArgs e)
        {
            this.rootFrame.Navigate(typeof(SchedulesPage));
            rootSplitView.IsPaneOpen = false;
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            this.rootFrame.Navigate(typeof(SettingsPage));
            rootSplitView.IsPaneOpen = false;
        }
    }
}
