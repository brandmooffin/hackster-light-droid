﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Light_Droid.Validations
{
    public class PasswordValidation
    {
        public static bool IsPasswordValid(string password)
        {
            return password.Length >= 6;
        }
    }
}
