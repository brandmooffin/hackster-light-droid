﻿using Windows.Storage;

namespace Light_Droid.Settings
{
    public class AppSettings
    {
        public int UserId = 0;
        public string UserEmail = string.Empty;
        public string PhillipsHueUsernameToken = "33a814b5193c1b66cb313c727240b10";
        public string PhillipsHueBridgeIpAddress = "10.10.10.125";
        public string LifxAccessToken = "c4820c66d34488233d27d0afde2ae27c43960748dc7fce7c19cdc29137bd3d52";

        public AppSettings()
        {
            LoadSettings();
        }

        public void SaveSettings()
        {
            ApplicationData.Current.LocalSettings.Values["UserId"] = UserId;
            ApplicationData.Current.LocalSettings.Values["UserEmail"] = UserEmail;
            ApplicationData.Current.LocalSettings.Values["LifxAccessToken"] = LifxAccessToken;
            ApplicationData.Current.LocalSettings.Values["PhillipsHueUsernameToken"] = PhillipsHueUsernameToken;
            ApplicationData.Current.LocalSettings.Values["PhillipsHueBridgeIpAddress"] = PhillipsHueBridgeIpAddress;
        }

        public void LoadSettings()
        {
            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("UserId"))
            {
                UserId = (int) ApplicationData.Current.LocalSettings.Values["UserId"];
            }

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("UserEmail"))
            {
                UserEmail = (string) ApplicationData.Current.LocalSettings.Values["UserEmail"];
            }

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("LifxAccessToken"))
            {
                LifxAccessToken = (string)ApplicationData.Current.LocalSettings.Values["LifxAccessToken"];
            }

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("PhillipsHueUsernameToken"))
            {
                PhillipsHueUsernameToken = (string)ApplicationData.Current.LocalSettings.Values["PhillipsHueUsernameToken"];
            }

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("PhillipsHueBridgeIpAddress"))
            {
                PhillipsHueBridgeIpAddress = (string)ApplicationData.Current.LocalSettings.Values["PhillipsHueBridgeIpAddress"];
            }
        }
    }
}
