﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Light_Droid.Navigation;

namespace Light_Droid.Views
{
    public sealed partial class SettingsPage : Page
    {
        public SettingsPage()
        {
            this.InitializeComponent();
        }

        private void GeneralButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationRootPage.RootFrame.Navigate(typeof(GeneralPage));
        }

        private void LifxButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationRootPage.RootFrame.Navigate(typeof(LifxPage));
        }

        private void PhillipsHueButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationRootPage.RootFrame.Navigate(typeof(PhillipsHuePage));
        }
    }
}
