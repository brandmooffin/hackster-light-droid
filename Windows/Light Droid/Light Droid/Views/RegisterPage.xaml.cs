﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Light_Droid.Models;
using Light_Droid.Navigation;
using Light_Droid.Validations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Light_Droid.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RegisterPage : Page
    {
        public RegisterPage()
        {
            this.InitializeComponent();
        }

        private void SignInButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationRootPage.RootFrame.Navigate(typeof(LoginPage));
        }

        private async void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            MainProgressRing.IsActive = true;
            if (PasswordValidation.IsPasswordValid(PasswordTextbox.Password) && PasswordTextbox.Password == ConfirmPasswordTextbox.Password)
            {
                if (EmailValidation.IsEmailValid(UsernameTextbox.Text))
                {
                    string url = App.ServiceBaseUrl + "IsEmailRegistered/" + UsernameTextbox.Text;
                    var request = HttpWebRequest.Create(url);
                    request.BeginGetResponse(ResponseIsEmailRegisteredServiceCallback, request);
                }
                else
                {
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                    {
                        MainProgressRing.IsActive = false;
                        var emailInvalidDialog =
                               new MessageDialog(
                                   "Please check your email and try again.",
                                   "Invalid Email");
                        await emailInvalidDialog.ShowAsync();
                    });
                }
            }
            else
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                {
                    MainProgressRing.IsActive = false;
                    var passwordInvalidDialog =
                           new MessageDialog(
                               "Password must be at least 6 characters. Please check your password and try again.",
                               "Invalid Password");
                    await passwordInvalidDialog.ShowAsync();
                });
            }
        }

        private async void ResponseIsEmailRegisteredServiceCallback(IAsyncResult result)
        {
            var request = (HttpWebRequest) result.AsyncState;
            var response = (HttpWebResponse) request.EndGetResponse(result);
            if (bool.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()))
            {
                // email is use
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                {
                    MainProgressRing.IsActive = false;
                    var emailInUseDialog =
                        new MessageDialog(
                            "Looks like this email is already in use. Please check your email and try again.",
                            "Email Already Used");
                    await emailInUseDialog.ShowAsync();
                });
                return;
            }

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                // register user
                if (await RegisterUser())
                {
                    MainProgressRing.IsActive = false;
                    var registeredDialog =
                        new MessageDialog(
                            "Please go to the login page and sign in.",
                            "Registered Successfully");
                    await registeredDialog.ShowAsync();

                }

                MainProgressRing.IsActive = false;
                var failedDialog =
                    new MessageDialog(
                        "Looks like registration has failed. Please check your connection and try again.",
                        "Registration Failed");
                await failedDialog.ShowAsync();
            });
        }

        private async Task<bool> RegisterUser()
        {
            string url = App.ServiceBaseUrl + "RegisterUser";
            var user = new User
            {
                Email = UsernameTextbox.Text,
                Password = PasswordTextbox.Password
            };

            byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(user));

            var request = HttpWebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            using (Stream stream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null))
            {
                stream.Write(data, 0, data.Length);
            }
            var response = await request.GetResponseAsync();
            return bool.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd());
        }
    }
}
