﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Light_Droid.Models;
using Light_Droid.Settings;
using Newtonsoft.Json.Linq;

namespace Light_Droid.Views
{
    public sealed partial class SchedulesPage : Page
    {
        List<Schedule> Schedules = new List<Schedule>();
        AppSettings appSettings = new AppSettings();
        public SchedulesPage()
        {
            this.InitializeComponent();
            Loaded += SchedulesPage_Loaded;
        }

        private void SchedulesPage_Loaded(object sender, RoutedEventArgs e)
        {
            MainProgressRing.IsActive = true;
            string url = App.ServiceBaseUrl + "GetSchedulesForUser?userId=" + appSettings.UserId;
            var request = HttpWebRequest.Create(url);
            request.BeginGetResponse(ResponseGetRoomsServiceCallback, request);
        }

        private async void ResponseGetRoomsServiceCallback(IAsyncResult result)
        {
            var request = (HttpWebRequest)result.AsyncState;
            var response = (HttpWebResponse)request.EndGetResponse(result);

            Schedules = JArray.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()).ToObject<List<Schedule>>();

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                MainProgressRing.IsActive = false;
                if (Schedules.Any())
                {
                    SchedulesListView.ItemsSource = Schedules;
                }
                else
                {
                    var getLightsFailedDialog =
                        new MessageDialog(
                            "Could not get schedules. Please check your Network Settings and verify that is connected then try again.",
                            "Couldn't Get Schedules");
                    await getLightsFailedDialog.ShowAsync();
                }
            });
        }

        private void SchedulesListView_OnItemClick(object sender, ItemClickEventArgs e)
        {
            Schedule schedule = (Schedule)e.ClickedItem;
            this.Frame.Navigate(typeof(ScheduleDetailsPage), schedule);
        }
    }
}
