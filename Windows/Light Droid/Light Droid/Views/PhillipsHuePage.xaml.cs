﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Light_Droid.Models;
using Light_Droid.Settings;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Q42.HueApi;
using Q42.HueApi.Interfaces;
using Light = Light_Droid.Models.Light;

namespace Light_Droid.Views
{
    public sealed partial class PhillipsHuePage : Page
    {
        AppSettings appSettings = new AppSettings();
        
        public PhillipsHuePage()
        {
            this.InitializeComponent();

            UsernameTokenTextBox.Text = appSettings.PhillipsHueUsernameToken;
            BridgeIpAddressTextBox.Text = appSettings.PhillipsHueBridgeIpAddress;
        }

        private async void ImportPhillipsHueButton_Click(object sender, RoutedEventArgs e)
        {
            MainProgressRing.IsActive = true;
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                ILocalHueClient client = new LocalHueClient(BridgeIpAddressTextBox.Text);
                client.Initialize(UsernameTokenTextBox.Text);

                var lights = await client.GetLightsAsync();

                var phillipsHueLights = lights as IList<Q42.HueApi.Light> ?? lights.ToList();
                if (phillipsHueLights.Any())
                {
                    appSettings.PhillipsHueUsernameToken = UsernameTokenTextBox.Text;
                    appSettings.PhillipsHueBridgeIpAddress = BridgeIpAddressTextBox.Text;
                    appSettings.SaveSettings();

                    foreach (var phillipsHueLight in phillipsHueLights)
                    {
                        Light light = new Light();
                        light.ExternalId = phillipsHueLight.Id;
                        light.Name = phillipsHueLight.Name;
                        light.UserId = appSettings.UserId;
                        light.IsOn = false;
                        light.BulbType = 2;
                        if (phillipsHueLight.State.On)
                        {
                            light.IsOn = true;
                        }

                        // add light
                        light = await AddLight(light);
                        if (light.Id != 0) continue;
                        var addLightFailedDialog =
                            new MessageDialog("Unable to add Phillips Hue light bulb. Please try again later.","Failed to Add Light");
                        await addLightFailedDialog.ShowAsync();
                        break;
                    }
                }
                MainProgressRing.IsActive = false;
            });
        }

        private async Task<Light> AddLight(Light light)
        {
            string url = App.ServiceBaseUrl + "AddLight";

            byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(light));

            var request = HttpWebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            using (Stream stream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null))
            {
                stream.Write(data, 0, data.Length);
            }
            var response = await request.GetResponseAsync();

            light = JObject.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()).ToObject<Light>();

            return light;
        }
    }
}
