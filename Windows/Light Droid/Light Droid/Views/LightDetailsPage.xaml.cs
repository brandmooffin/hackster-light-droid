﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Light_Droid.Models;
using Newtonsoft.Json;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Light_Droid.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LightDetailsPage : Page
    {
        private Light Light;
        public LightDetailsPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // Register for hardware and software back request from the system
            SystemNavigationManager systemNavigationManager = SystemNavigationManager.GetForCurrentView();
            systemNavigationManager.BackRequested += OnBackRequested;
            systemNavigationManager.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;

            if (e.Parameter != null)
            {
                Light = (Light)e.Parameter;
                LightNameTextBlock.Text = Light.Name;
                LightIsOnTextBlock.Text = Light.IsOnDescription;
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            SystemNavigationManager systemNavigationManager = SystemNavigationManager.GetForCurrentView();
            systemNavigationManager.BackRequested -= OnBackRequested;
            systemNavigationManager.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            // Mark event as handled so we don't get bounced out of the app.
            e.Handled = true;
            // Page above us will be our master view.
            // Make sure we are using the "drill out" animation in this transition.
            Frame.GoBack(new DrillInNavigationTransitionInfo());
        }

        private void AddScheduleButton_OnClick(object sender, RoutedEventArgs e)
        {
            Schedule schedule = new Schedule
            {
                EntityId = Light.Id,
                EntityType = 2
            };
            this.Frame.Navigate(typeof(AddSchedulePage), schedule);
        }

        private void ToggleLightButton_OnClick(object sender, RoutedEventArgs e)
        {
            Light.IsOn = !Light.IsOn;

            UpdateLight();
            ToggleLight();

            LightIsOnTextBlock.Text = Light.IsOnDescription;
        }

        private async void UpdateLight()
        {
            string url = App.ServiceBaseUrl + "UpdateLight";
            byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(Light));

            var request = HttpWebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            using (Stream stream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null))
            {
                stream.Write(data, 0, data.Length);
            }
            var response = await request.GetResponseAsync();
        }

        private async void ToggleLight()
        {
            int lightAction = 1;
            if (!Light.IsOn)
            {
                lightAction = 2;
            }

            string url = App.ServiceBaseUrl + "ToggleLight?lightId="+Light.Id+"&action="+lightAction;
            var request = HttpWebRequest.Create(url);
            var response = await request.GetResponseAsync();
        }

        private async void UnregisterLightButton_OnClick(object sender, RoutedEventArgs e)
        {
            string url = App.ServiceBaseUrl + "RemoveLight?lightId=" + Light.Id;
            var request = HttpWebRequest.Create(url);
            var response = await request.GetResponseAsync();
            if (bool.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()))
            {
                Frame.GoBack(new DrillInNavigationTransitionInfo());
            }
            else
            {
                var removeLightFailedDialog =
                        new MessageDialog(
                            "Could not unregister light. Please check your Network Settings and verify that is connected then try again.",
                            "Couldn't Unregister Light");
                await removeLightFailedDialog.ShowAsync();
            }
        }
    }
}
