﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Light_Droid.Models;
using Light_Droid.Settings;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Light_Droid.Views
{
    public sealed partial class AddSchedulePage : Page
    {
        Schedule Schedule;
        AppSettings appSettings = new AppSettings();
        public AddSchedulePage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // Register for hardware and software back request from the system
            SystemNavigationManager systemNavigationManager = SystemNavigationManager.GetForCurrentView();
            systemNavigationManager.BackRequested += OnBackRequested;
            systemNavigationManager.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;

            if (e.Parameter != null)
            {
                Schedule = (Schedule)e.Parameter;
                Schedule.UserId = appSettings.UserId;
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            SystemNavigationManager systemNavigationManager = SystemNavigationManager.GetForCurrentView();
            systemNavigationManager.BackRequested -= OnBackRequested;
            systemNavigationManager.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            // Mark event as handled so we don't get bounced out of the app.
            e.Handled = true;
            // Page above us will be our master view.
            // Make sure we are using the "drill out" animation in this transition.
            Frame.GoBack(new DrillInNavigationTransitionInfo());
        }

        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if ((!TimeSetRadio.IsChecked??false) && (!SunsetRadio.IsChecked??false) && (!SunriseRadio.IsChecked??false))
            {
                var selectTimeDialog =
                       new MessageDialog(
                           "You must select a time to add a schedule.",
                           "Select Time");
                await selectTimeDialog.ShowAsync();
                return;
            }


            Schedule.Action = 2;
            if (ActionOnRadio.IsChecked ?? false)
            {
                Schedule.Action = 1;
            }

            var executeTime = ExecuteTimePicker.Time.Hours + ":" + ExecuteTimePicker.Time.Minutes;
            Schedule.ExecuteTime = executeTime;
            Schedule.Type = 0;
            if (TimeSetRadio.IsChecked ?? false)
            {
                Schedule.Type = 1;
            }else if (SunsetRadio.IsChecked ?? false)
            {
                Schedule.Type = 2;
            }else if (SunriseRadio.IsChecked ?? false)
            {
                Schedule.Type = 3;
            }
            
            if (await AddSchedule())
            {
                var scheduleSuccessDialog =
                        new MessageDialog(
                            "Schedule has been successfully added.",
                            "Schedule Added");
                await scheduleSuccessDialog.ShowAsync();
            }
            else
            {
                var getLightsFailedDialog =
                        new MessageDialog(
                            "Could not add schedule. Please check your Network Settings and verify that is connected then try again.",
                            "Couldn't Add Schedule");
                await getLightsFailedDialog.ShowAsync();
            }
        }

        private async Task<bool> AddSchedule()
        {
            string url = App.ServiceBaseUrl + "AddSchedule";
            byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(Schedule));

            var request = HttpWebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            using (Stream stream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null))
            {
                stream.Write(data, 0, data.Length);
            }
            var response = await request.GetResponseAsync();

            var schedule = JObject.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()).ToObject<Schedule>();
            return schedule.Id > 0;
        }

        private void TimeSetRadio_Checked(object sender, RoutedEventArgs e)
        {
            ExecuteTimePicker.Visibility = Visibility.Visible;
        }

        private void SunsetRadio_Checked(object sender, RoutedEventArgs e)
        {
            ExecuteTimePicker.Visibility = Visibility.Collapsed;
        }

        private void SunriseRadio_Checked(object sender, RoutedEventArgs e)
        {
            ExecuteTimePicker.Visibility = Visibility.Collapsed;
        }
    }
}
