﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Light_Droid.Models;
using Light_Droid.Settings;
using Newtonsoft.Json.Linq;

namespace Light_Droid.Views
{
    public sealed partial class LightsPage : Page
    {
        List<Light> Lights = new List<Light>(); 
        AppSettings appSettings = new AppSettings();
        public LightsPage()
        {
            this.InitializeComponent();

            Loaded += LightsPage_Loaded;
        }

        private void LightsPage_Loaded(object sender, RoutedEventArgs e)
        {
            MainProgressRing.IsActive = true;
            string url = App.ServiceBaseUrl + "GetLights?userId=" + appSettings.UserId;
            var request = HttpWebRequest.Create(url);
            request.BeginGetResponse(ResponseGetLightsServiceCallback, request);
        }

        private async void ResponseGetLightsServiceCallback(IAsyncResult result)
        {
            var request = (HttpWebRequest)result.AsyncState;
            var response = (HttpWebResponse)request.EndGetResponse(result);

            Lights = JArray.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()).ToObject<List<Light>>();

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                MainProgressRing.IsActive = false;
                if (Lights.Any())
                {
                    LightsListView.ItemsSource = Lights;
                }
                else
                {
                    var getLightsFailedDialog =
                        new MessageDialog(
                            "Could not get lights. Please check your Network Settings and verify that is connected then try again.",
                            "Couldn't Get Lights");
                    await getLightsFailedDialog.ShowAsync();
                }
            });
        }

        private void LightsListView_OnItemClick(object sender, ItemClickEventArgs e)
        {
            Light light = (Light)e.ClickedItem;
            this.Frame.Navigate(typeof(LightDetailsPage), light);
        }
    }
}
