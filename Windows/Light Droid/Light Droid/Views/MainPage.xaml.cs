﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Light_Droid.Navigation;
using Light_Droid.Views;

namespace Light_Droid
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void LightsButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationRootPage.RootFrame.Navigate(typeof(LightsPage));
        }

        private void RoomsButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationRootPage.RootFrame.Navigate(typeof(RoomsPage));
        }

        private void SchedulesButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationRootPage.RootFrame.Navigate(typeof(SchedulesPage));
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationRootPage.RootFrame.Navigate(typeof(SettingsPage));
        }
    }
}
