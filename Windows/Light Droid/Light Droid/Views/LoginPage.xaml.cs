﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Light_Droid.Models;
using Light_Droid.Navigation;
using Light_Droid.Settings;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Light_Droid.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginPage : Page
    {
        private AppSettings appSettings = new AppSettings();
        public LoginPage()
        {
            this.InitializeComponent();
        }

        private async void SignInButton_Click(object sender, RoutedEventArgs e)
        {
            MainProgressRing.IsActive = true;
            string url = App.ServiceBaseUrl + "Login";
            var user = new User
            {
                Email = UsernameTextbox.Text,
                Password = PasswordTextbox.Password
            };

            byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(user));

            var request = HttpWebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            using (Stream stream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null))
            {
                stream.Write(data, 0, data.Length);
            }
            request.BeginGetResponse(ResponseLoginServiceCallback, request);
        }

        private async void ResponseLoginServiceCallback(IAsyncResult result)
        {
            var request = (HttpWebRequest)result.AsyncState;
            var response = (HttpWebResponse)request.EndGetResponse(result);

            var user = JObject.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()).ToObject<User>();
            
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                MainProgressRing.IsActive = false;
                if (user.Id > 0)
                {
                    appSettings.UserId = user.Id;
                    appSettings.UserEmail = user.Email;
                    appSettings.SaveSettings();
                    NavigationRootPage.RootFrame.Navigate(typeof (MainPage));
                }
                else
                {
                    var loginFailedDialog =
                        new MessageDialog(
                            "Could not login. Please check your Network Settings and verify that is connected then try again.",
                            "Login Failed");
                    await loginFailedDialog.ShowAsync();
                }
            });
        }

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
           NavigationRootPage.RootFrame.Navigate(typeof(RegisterPage));
        }
    }
}
