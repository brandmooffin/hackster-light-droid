﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Light_Droid.Models;
using Newtonsoft.Json.Linq;

namespace Light_Droid.Views
{
    public sealed partial class RoomDetailsPage : Page
    {
        private Room Room;
        public RoomDetailsPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // Register for hardware and software back request from the system
            SystemNavigationManager systemNavigationManager = SystemNavigationManager.GetForCurrentView();
            systemNavigationManager.BackRequested += OnBackRequested;
            systemNavigationManager.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;

            if (e.Parameter != null)
            {
                Room = (Room)e.Parameter;
                RoomNameTextBlock.Text = Room.Name;
                
                MainProgressRing.IsActive = true;
                string url = App.ServiceBaseUrl + "GetLightsForRoom?roomId=" + Room.Id;
                var request = HttpWebRequest.Create(url);
                request.BeginGetResponse(ResponseGetLightsServiceCallback, request);
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            SystemNavigationManager systemNavigationManager = SystemNavigationManager.GetForCurrentView();
            systemNavigationManager.BackRequested -= OnBackRequested;
            systemNavigationManager.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
        }
        
        private async void ResponseGetLightsServiceCallback(IAsyncResult result)
        {
            var request = (HttpWebRequest)result.AsyncState;
            var response = (HttpWebResponse)request.EndGetResponse(result);

            var lights = JArray.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()).ToObject<List<Light>>();
            Room.Lights = lights;
            
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                MainProgressRing.IsActive = false;
                RoomIsOnTextBlock.Text = Room.IsOnDescription;
                if (lights.Any())
                {
                    LightsListView.ItemsSource = lights;
                }
                else
                {
                    var getLightsFailedDialog =
                        new MessageDialog(
                            "Could not get lights. Please check your Network Settings and verify that is connected then try again.",
                            "Couldn't Get Lights");
                    await getLightsFailedDialog.ShowAsync();
                }
            });
        }

        private void LightsListView_OnItemClick(object sender, ItemClickEventArgs e)
        {
            Light light = (Light)e.ClickedItem;
            this.Frame.Navigate(typeof(LightDetailsPage), light);
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            // Mark event as handled so we don't get bounced out of the app.
            e.Handled = true;
            // Page above us will be our master view.
            // Make sure we are using the "drill out" animation in this transition.
            Frame.GoBack(new DrillInNavigationTransitionInfo());
        }

        private void AddScheduleButton_OnClick(object sender, RoutedEventArgs e)
        {
            Schedule schedule = new Schedule
            {
                EntityId = Room.Id,
                EntityType = 1
            };
            this.Frame.Navigate(typeof(AddSchedulePage), schedule);
        }

        private void ToggleRoomButton_OnClick(object sender, RoutedEventArgs e)
        {
            ToggleRoom();
            MainProgressRing.IsActive = true;
            string url = App.ServiceBaseUrl + "GetLightsForRoom?roomId=" + Room.Id;
            var request = HttpWebRequest.Create(url);
            request.BeginGetResponse(ResponseGetLightsServiceCallback, request);
        }

        private async void ToggleRoom()
        {
            string url = App.ServiceBaseUrl + "ToggleRoom?roomId=" + Room.Id;
            var request = HttpWebRequest.Create(url);
            var response = await request.GetResponseAsync();
        }

        private async void UnregisterRoomButton_OnClick(object sender, RoutedEventArgs e)
        {
            string url = App.ServiceBaseUrl + "RemoveRoom?roomId=" + Room.Id;
            var request = HttpWebRequest.Create(url);
            var response = await request.GetResponseAsync();
            if (bool.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()))
            {
                Frame.GoBack(new DrillInNavigationTransitionInfo());
            }
            else
            {
                var removeRoomFailedDialog =
                        new MessageDialog(
                            "Could not unregister room. Please check your Network Settings and verify that is connected then try again.",
                            "Couldn't Unregister Room");
                await removeRoomFailedDialog.ShowAsync();
            }
        }
    }
}
