﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Light_Droid.Models;
using Light_Droid.Settings;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Light_Droid.Views
{
    public sealed partial class LifxPage : Page
    {
        AppSettings appSettings = new AppSettings();
        List<LifxLight> Lights = new List<LifxLight>();
        public LifxPage()
        {
            this.InitializeComponent();

            AccessTokenTextBox.Text = appSettings.LifxAccessToken;
        }

        private async void ImportLifxButton_Click(object sender, RoutedEventArgs e)
        {
            MainProgressRing.IsActive = true;
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                var url = "https://api.lifx.com/v1/lights/";
                var request = HttpWebRequest.Create(url);
                request.Method = "GET";
                request.Headers["Authorization"] = "Bearer " + AccessTokenTextBox.Text;
                request.ContentType = "application/json; charset=utf-8";
                var response = await request.GetResponseAsync();
                var schedulesJsonResponse = new StreamReader(response.GetResponseStream()).ReadToEnd();
                Lights.AddRange(JArray.Parse(schedulesJsonResponse).ToObject<List<LifxLight>>());

                if (Lights.Any())
                {
                    appSettings.LifxAccessToken = AccessTokenTextBox.Text;
                    appSettings.SaveSettings();

                    foreach (var lifxLight in Lights)
                    {
                        Light light = new Light();
                        Room room = new Room();
                        LifxGroup group = lifxLight.group;
                        light.ExternalId = lifxLight.id;
                        light.Name = lifxLight.label;
                        light.UserId = appSettings.UserId;
                        light.IsOn = false;
                        light.BulbType = 1;
                        if (lifxLight.power.ToLower().Equals("on"))
                        {
                            light.IsOn = true;
                        }

                        // add light
                        light = await AddLight(light);

                        if (group != null && !string.IsNullOrEmpty(group.id))
                        {
                            // add group
                            room.ExternalId = group.id;
                            room.Name = group.name;
                            room.UserId = appSettings.UserId;

                            room = await AddRoom(room);
                        }

                        if (light.Id > 0 && room.Id > 0)
                        {
                            // add link
                            if (!await AddLightToRoom(room.Id, light.Id))
                            {
                                // something went wrong
                            }
                        }
                    }
                }
                MainProgressRing.IsActive = false;
            });
        }

        private async Task<bool> AddLightToRoom(int roomId, int lightId)
        {
            string url = App.ServiceBaseUrl + "AddLightToRoom?roomId=" + roomId + "&lightId=" + lightId;
            var request = HttpWebRequest.Create(url);
            var response = await request.GetResponseAsync();
            return bool.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd());
        }

        private async Task<Room> AddRoom(Room room)
        {
            string url = App.ServiceBaseUrl + "AddRoom";

            byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(room));

            var request = HttpWebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            using (Stream stream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null))
            {
                stream.Write(data, 0, data.Length);
            }
            var response = await request.GetResponseAsync();

            room = JObject.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()).ToObject<Room>();

            return room;
        }

        private async Task<Light> AddLight(Light light)
        {
            string url = App.ServiceBaseUrl + "AddLight";

            byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(light));

            var request = HttpWebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            using (Stream stream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null))
            {
                stream.Write(data, 0, data.Length);
            }
            var response = await request.GetResponseAsync();

            light = JObject.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()).ToObject<Light>();

            return light;
        }
    }
}
