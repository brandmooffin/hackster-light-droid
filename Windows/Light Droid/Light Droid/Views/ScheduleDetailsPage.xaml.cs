﻿using System;
using System.IO;
using System.Net;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Light_Droid.Models;

namespace Light_Droid.Views
{
    public sealed partial class ScheduleDetailsPage : Page
    {
        Schedule Schedule;
        public ScheduleDetailsPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // Register for hardware and software back request from the system
            SystemNavigationManager systemNavigationManager = SystemNavigationManager.GetForCurrentView();
            systemNavigationManager.BackRequested += OnBackRequested;
            systemNavigationManager.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;

            if (e.Parameter != null)
            {
                Schedule = (Schedule)e.Parameter;
                SchedulesDescriptionTextBlock.Text = Schedule.EntityName + " @ " + Schedule.ExecuteTimeDescription;
                SchedulesActionTextBlock.Text = Schedule.ActionDescription;
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            SystemNavigationManager systemNavigationManager = SystemNavigationManager.GetForCurrentView();
            systemNavigationManager.BackRequested -= OnBackRequested;
            systemNavigationManager.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            // Mark event as handled so we don't get bounced out of the app.
            e.Handled = true;
            // Page above us will be our master view.
            // Make sure we are using the "drill out" animation in this transition.
            Frame.GoBack(new DrillInNavigationTransitionInfo());
        }

        private async void UnregisterScheduleButton_OnClick(object sender, RoutedEventArgs e)
        {
            string url = App.ServiceBaseUrl + "RemoveSchedule?scheduleId=" + Schedule.Id;
            var request = HttpWebRequest.Create(url);
            var response = await request.GetResponseAsync();
            if (bool.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()))
            {
                Frame.GoBack(new DrillInNavigationTransitionInfo());
            }
            else
            {
                var removeScheduleFailedDialog =
                        new MessageDialog(
                            "Could not unregister schedule. Please check your Network Settings and verify that is connected then try again.",
                            "Couldn't Unregister Schedule");
                await removeScheduleFailedDialog.ShowAsync();
            }
        }
    }
}
