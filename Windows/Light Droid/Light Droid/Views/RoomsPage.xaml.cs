﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Light_Droid.Models;
using Light_Droid.Settings;
using Newtonsoft.Json.Linq;

namespace Light_Droid.Views
{
    public sealed partial class RoomsPage : Page
    {
        List<Room> Rooms = new List<Room>();
        AppSettings appSettings = new AppSettings();
        public RoomsPage()
        {
            this.InitializeComponent();

            Loaded += RoomsPage_Loaded;
        }

        private void RoomsPage_Loaded(object sender, RoutedEventArgs e)
        {
            MainProgressRing.IsActive = true;
            string url = App.ServiceBaseUrl + "GetRooms?userId=" + appSettings.UserId;
            var request = HttpWebRequest.Create(url);
            request.BeginGetResponse(ResponseGetRoomsServiceCallback, request);
        }

        private async void ResponseGetRoomsServiceCallback(IAsyncResult result)
        {
            var request = (HttpWebRequest)result.AsyncState;
            var response = (HttpWebResponse)request.EndGetResponse(result);

            Rooms = JArray.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()).ToObject<List<Room>>();

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                MainProgressRing.IsActive = false;
                if (Rooms.Any())
                {
                    RoomsListView.ItemsSource = Rooms;
                }
                else
                {
                    var getLightsFailedDialog =
                        new MessageDialog(
                            "Could not get romms. Please check your Network Settings and verify that is connected then try again.",
                            "Couldn't Get Rooms");
                    await getLightsFailedDialog.ShowAsync();
                }
            });
        }

        private void RoomsListView_OnItemClick(object sender, ItemClickEventArgs e)
        {
            Room room = (Room)e.ClickedItem;
            this.Frame.Navigate(typeof(RoomDetailsPage), room);
        }
    }
}
