package apps.brokenwallsstudios.lightdroid.model;

/**
 * Created by brand on 1/8/2016.
 */
public class Schedule {
    public int Id;
    public String ExecuteTime;
    public int Action;
    public int Type;
    public int EntityType;
    public int EntityId;
    public String EntityName;
}
