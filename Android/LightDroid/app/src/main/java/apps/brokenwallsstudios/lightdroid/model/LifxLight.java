package apps.brokenwallsstudios.lightdroid.model;

/**
 * Created by Brandon on 1/20/2016.
 */
public class LifxLight {
    public String id;
    public String uuid;
    public String label;
    public boolean connected;
    public String power;
    //color
    public int brightness;
    public LifxGroup group;
    //location
    //product
    public String last_seen;
    public double seconds_since_seen;
}