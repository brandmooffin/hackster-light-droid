﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="LightDroidService.Azure" generation="1" functional="0" release="0" Id="a264d569-11e0-4855-a313-bbeba3d700ce" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="LightDroidService.AzureGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="LightDroidService:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/LightDroidService.Azure/LightDroidService.AzureGroup/LB:LightDroidService:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="LightDroidService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/LightDroidService.Azure/LightDroidService.AzureGroup/MapLightDroidService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="LightDroidServiceInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/LightDroidService.Azure/LightDroidService.AzureGroup/MapLightDroidServiceInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:LightDroidService:Endpoint1">
          <toPorts>
            <inPortMoniker name="/LightDroidService.Azure/LightDroidService.AzureGroup/LightDroidService/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapLightDroidService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/LightDroidService.Azure/LightDroidService.AzureGroup/LightDroidService/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapLightDroidServiceInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/LightDroidService.Azure/LightDroidService.AzureGroup/LightDroidServiceInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="LightDroidService" generation="1" functional="0" release="0" software="C:\Projects\Light Droid\LightDroidService\LightDroidService.Azure\csx\Release\roles\LightDroidService" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;LightDroidService&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;LightDroidService&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/LightDroidService.Azure/LightDroidService.AzureGroup/LightDroidServiceInstances" />
            <sCSPolicyUpdateDomainMoniker name="/LightDroidService.Azure/LightDroidService.AzureGroup/LightDroidServiceUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/LightDroidService.Azure/LightDroidService.AzureGroup/LightDroidServiceFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="LightDroidServiceUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="LightDroidServiceFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="LightDroidServiceInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="b21deb30-e872-460f-bea0-31a0a6a1e2fd" ref="Microsoft.RedDog.Contract\ServiceContract\LightDroidService.AzureContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="64623ae0-ad54-45e0-9065-69b19dd00bcc" ref="Microsoft.RedDog.Contract\Interface\LightDroidService:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/LightDroidService.Azure/LightDroidService.AzureGroup/LightDroidService:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>