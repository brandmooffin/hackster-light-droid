﻿using System;
using System.IO;
using System.Net;
using LightDroidService.Models;
using Newtonsoft.Json.Linq;

namespace LightDroidService.Connectors
{
    public class OpenWeatherConnector
    {
        private static readonly string easternZoneId = "Eastern Standard Time";

        public static string GetSunset(string location)
        {
            string url = "http://api.openweathermap.org/data/2.5/weather?q=" + location +
                         "&APPID=fac89f65b18f4a6c47596246f45b9359&units=imperial";
            var request = HttpWebRequest.Create(url);
            var response = request.GetResponse();
            var openWeatherData = JObject.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()).ToObject<OpenWeatherData>();
            int sunsetUnixTimeStamp = openWeatherData.sys.sunset;
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById(easternZoneId);
            dtDateTime = dtDateTime.AddSeconds(sunsetUnixTimeStamp);
            dtDateTime = TimeZoneInfo.ConvertTimeFromUtc(dtDateTime, easternZone);
            return $"{dtDateTime.Hour}:{dtDateTime.Minute}";
        }

        public static string GetSunrise(string location)
        {
            string url = "http://api.openweathermap.org/data/2.5/weather?q=" + location +
                         "&APPID=fac89f65b18f4a6c47596246f45b9359&units=imperial";
            var request = HttpWebRequest.Create(url);
            var response = request.GetResponse();
            var openWeatherData = JObject.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd()).ToObject<OpenWeatherData>();
            int sunsetUnixTimeStamp = openWeatherData.sys.sunrise;
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(sunsetUnixTimeStamp);
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById(easternZoneId);
            dtDateTime = dtDateTime.AddSeconds(sunsetUnixTimeStamp);
            dtDateTime = TimeZoneInfo.ConvertTimeFromUtc(dtDateTime, easternZone);

            return $"{dtDateTime.Hour}:{dtDateTime.Minute}";
        }
    }
}