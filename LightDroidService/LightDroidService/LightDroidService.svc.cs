﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web.Script.Serialization;
using LightDroidService.Connectors;
using LightDroidService.Data;
using LightDroidService.Models;

namespace LightDroidService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class LightDroidService : ILightDroidService
    {
        [WebInvoke(Method = "POST", UriTemplate = "RegisterUser", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool RegisterUser(Stream userStream)
        {
            try
            {
                StreamReader streamReader = new StreamReader(userStream);
                var streamStr = streamReader.ReadToEnd();
                var user = new JavaScriptSerializer().Deserialize<User>(streamStr);

                // Validate that the password meets the required length; this is my preference and you can totally change it
                if (user.Password.Length < 6) return false;

                // Register user account
                return LightDroidDataManager.RegisterUser(user);
            }
            catch (Exception ex)
            {
                // Log error message
                //Email.ErrorLogEmail("RegisterUser: " + ex.Message);
            }

            return false;
        }

        [WebGet(UriTemplate = "IsEmailRegistered/{email}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool IsEmailRegistered(string email)
        {
            return LightDroidDataManager.IsEmailRegistered(email);
        }

        [WebInvoke(Method = "POST", UriTemplate = "Login", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public User Login(Stream userStream)
        {
            User user = null;
            try
            {
                StreamReader streamReader = new StreamReader(userStream);
                var streamStr = streamReader.ReadToEnd();
                user = new JavaScriptSerializer().Deserialize<User>(streamStr);

                user = LightDroidDataManager.LoginUser(user);
            }
            catch (Exception ex)
            {
                //Email.ErrorLogEmail("RegisterUser: " + ex.Message);
            }

            return user;
        }

        [WebInvoke(Method = "POST", UriTemplate = "AddLight", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public Light AddLight(Stream lightStream)
        {
            var streamReader = new StreamReader(lightStream);
            var streamStr = streamReader.ReadToEnd();
            var light = new JavaScriptSerializer().Deserialize<Light>(streamStr);

            return LightDroidDataManager.AddLight(light);
        }

        [WebInvoke(Method = "POST", UriTemplate = "AddRoom", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public Room AddRoom(Stream roomStream)
        {
            var streamReader = new StreamReader(roomStream);
            var streamStr = streamReader.ReadToEnd();
            var room = new JavaScriptSerializer().Deserialize<Room>(streamStr);

            return LightDroidDataManager.AddRoom(room);
        }

        [WebInvoke(Method = "POST", UriTemplate = "AddSchedule", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public Schedule AddSchedule(Stream scheduleStream)
        {
            var streamReader = new StreamReader(scheduleStream);
            var streamStr = streamReader.ReadToEnd();
            var schedule = new JavaScriptSerializer().Deserialize<Schedule>(streamStr);

            return LightDroidDataManager.AddSchedule(schedule);
        }

        [WebGet(UriTemplate = "GetLights?userId={userId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<Light> GetLights(int userId)
        {
            return LightDroidDataManager.GetLights(userId);
        }

        [WebGet(UriTemplate = "GetRooms?userId={userId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<Room> GetRooms(int userId)
        {
            return LightDroidDataManager.GetRooms(userId);
        }
        
        [WebGet(UriTemplate = "GetLight?lightId={lightId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public Light GetLight(int lightId)
        {
            return LightDroidDataManager.GetLight(lightId);
        }

        [WebGet(UriTemplate = "GetRoom?roomId={roomId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public Room GetRoom(int roomId)
        {
            return LightDroidDataManager.GetRoom(roomId);
        }

        [WebGet(UriTemplate = "GetSchedule?scheduleId={scheduleId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public Schedule GetSchedule(int scheduleId)
        {
            return LightDroidDataManager.GetSchedule(scheduleId);
        }

        [WebGet(UriTemplate = "GetLightsForRoom?roomId={roomId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<Light> GetLightsForRoom(int roomId)
        {
            return LightDroidDataManager.GetLightsForRoom(roomId);
        }

        [WebGet(UriTemplate = "GetSchedulesForRoom?roomId={roomId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<Schedule> GetSchedulesForRoom(int roomId)
        {
            return LightDroidDataManager.GetSchedulesForRoom(roomId);
        }

        [WebGet(UriTemplate = "GetSchedulesForLight?lightId={lightId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<Schedule> GetSchedulesForLight(int lightId)
        {
            return LightDroidDataManager.GetSchedulesForLight(lightId);
        }

        [WebGet(UriTemplate = "GetSchedulesForUser?userId={userId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<Schedule> GetSchedulesForUser(int userId)
        {
            return LightDroidDataManager.GetSchedulesForUser(userId);
        }

        [WebGet(UriTemplate = "GetSchedulesForUserWithPhillipsHue?userId={userId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<Schedule> GetSchedulesForUserWithPhillipsHue(int userId)
        {
            return LightDroidDataManager.GetSchedulesForUserWithPhillipsHue(userId);
        }

        [WebGet(UriTemplate = "GetSchedulesForUserWithPhoton?userId={userId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<PhotonSchedule> GetSchedulesForUserWithPhoton(int userId)
        {
            return LightDroidDataManager.GetSchedulesForUserWithPhoton(userId);
        }

        [WebGet(UriTemplate = "GetSchedulesForUserWithPhotonAndPhillipsHue?userId={userId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<PhotonPhillipsHueSchedule> GetSchedulesForUserWithPhotonAndPhillipsHue(int userId)
        {
            return LightDroidDataManager.GetSchedulesForUserWithPhotonAndPhillipsHue(userId);
        }

        [WebGet(UriTemplate = "RemoveLight?lightId={lightId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool RemoveLight(int lightId)
        {
            return LightDroidDataManager.RemoveLight(lightId);
        }

        [WebGet(UriTemplate = "RemoveRoom?roomId={roomId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool RemoveRoom(int roomId)
        {
            return LightDroidDataManager.RemoveRoom(roomId);
        }

        [WebGet(UriTemplate = "RemoveSchedule?scheduleId={scheduleId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool RemoveSchedule(int scheduleId)
        {
            return LightDroidDataManager.RemoveSchedule(scheduleId);
        }

        [WebInvoke(Method = "POST", UriTemplate = "UpdateLight", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool UpdateLight(Stream lightStream)
        {
            var streamReader = new StreamReader(lightStream);
            var streamStr = streamReader.ReadToEnd();
            var light = new JavaScriptSerializer().Deserialize<Light>(streamStr);

            return LightDroidDataManager.UpdateLight(light);
        }

        [WebInvoke(Method = "POST", UriTemplate = "UpdateRoom", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool UpdateRoom(Stream roomStream)
        {
            var streamReader = new StreamReader(roomStream);
            var streamStr = streamReader.ReadToEnd();
            var room = new JavaScriptSerializer().Deserialize<Room>(streamStr);

            return LightDroidDataManager.UpdateRoom(room);
        }

        [WebInvoke(Method = "POST", UriTemplate = "UpdateSchedule", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool UpdateSchedule(Stream scheduleStream)
        {
            var streamReader = new StreamReader(scheduleStream);
            var streamStr = streamReader.ReadToEnd();
            var schedule = new JavaScriptSerializer().Deserialize<Schedule>(streamStr);

            return LightDroidDataManager.UpdateSchedule(schedule);
        }

        [WebGet(UriTemplate = "ExecuteSchedule?scheduleId={scheduleId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public void ExecuteSchedule(int scheduleId)
        {
            LightDroidDataManager.ExecuteSchedule(scheduleId);
        }

        [WebGet(UriTemplate = "ToggleLight?lightId={lightId}&action={action}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public void ToggleLight(int lightId, int action)
        {
            LightDroidDataManager.ToggleLight(lightId, action);
        }

        [WebGet(UriTemplate = "ToggleRoom?roomId={roomId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public void ToggleRoom(int roomId)
        {
            LightDroidDataManager.ToggleRoom(roomId);
        }

        [WebGet(UriTemplate = "AddLightToRoom?roomId={roomId}&lightId={lightId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool AddLightToRoom(int roomId, int lightId)
        {
            return LightDroidDataManager.AddLightToRoom(roomId, lightId);
        }

        [WebGet(UriTemplate = "GetSunsetTime?location={location}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string GetSunsetTime(string location)
        {
            return OpenWeatherConnector.GetSunset(location);
        }

        [WebGet(UriTemplate = "UpdateSunsetSchedules?userId={userId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool UpdateSunsetSchedules(int userId)
        {
            return LightDroidDataManager.UpdateSunsetSchedules(userId);
        }

        [WebGet(UriTemplate = "UpdateSunriseSchedules?userId={userId}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool UpdateSunriseSchedules(int userId)
        {
            return LightDroidDataManager.UpdateSunriseSchedules(userId);
        }
    }
}
