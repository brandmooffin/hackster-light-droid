﻿namespace LightDroidService.Models
{
    public class PhotonPhillipsHueSchedule
    {
        public int Action { get; set; }
        public string ExternalId { get; set; }
        public string ExecuteTime { get; set; }
    }
}
