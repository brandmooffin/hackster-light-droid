﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightDroidService.Models
{
    public class Schedule
    {
        public int Id { get; set; }

        public string ExecuteTime { get; set; }

        public int Action { get; set; }

        public int Type { get; set; }

        public int EntityType { get; set; }

        public int EntityId { get; set; }

        public string EntityName { get; set; }

        public int UserId { get; set; }
    }
}
