﻿namespace LightDroidService.Models
{
    public class ScheduleType
    {
        public static int TimeSet = 1;
        public static int Sunset = 2;
        public static int Sunrise = 3;
    }
}