// This #include statement was automatically added by the Particle IDE.
#include "SparkTime/SparkTime.h"

// This #include statement was automatically added by the Particle IDE.
#include "SparkJson/SparkJson.h"

// This #include statement was automatically added by the Particle IDE.
#include "HttpClient/HttpClient.h"

/**
* Declaring the variables.
*/
unsigned int nextTime = 0;    // Next time to contact the server
HttpClient http;

UDP UDPClient;
SparkTime rtc;

unsigned long currentTime;

int led1 = D7;

// Headers currently need to be set at init, useful for API keys etc.
http_header_t headers[] = {
    //{ "Content-Type", "application/json" },
    //  { "Accept" , "application/json" },
    { "Accept" , "*/*"},
    { NULL, NULL } // NOTE: Always terminate headers will NULL
};

http_request_t request;
http_response_t response;

void setup() {
    rtc.begin(&UDPClient, "north-america.pool.ntp.org");
    pinMode(led1, OUTPUT);
    Serial.begin(9600);
}

void loop() {
    if (nextTime > millis()) {
        return;
    }

    Particle.publish("Application>\tStart of Loop.");

    request.hostname = "lightdroidservice.cloudapp.net";
    request.port = 80;

    currentTime = rtc.now();
    int currentHour = rtc.hour(currentTime);
    int currentMinute = rtc.minute(currentTime);

    if(currentHour == 3 && currentMinute == 0){
        request.path = "/LightDroidService/UpdateSunsetSchedules?userId=1";
        http.get(request, response, headers);
        request.path = "/LightDroidService/UpdateSunriseSchedules?userId=1";
        http.get(request, response, headers);
    }

    request.path = "/LightDroidService/GetSchedulesForUserWithPhoton?userId=1";

    // Get request
    http.get(request, response, headers);

    String url = response.body;

    digitalWrite(led1, HIGH);

    String cleanedUpJson = url.replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"ExecuteTime\"", "").replace("\"Id\"", "").replace(":", ",").replace(",,", ",").replace("\"", "");

    char cleanJsonChar[cleanedUpJson.length() + 1];
    strcpy(cleanJsonChar, cleanedUpJson);

    char * pch = strtok (cleanJsonChar,",");

    while (pch != NULL)
    {
        char* scheduleExecuteTimeHour = pch;
        pch = strtok (NULL, ",");
        char* scheduleExecuteTimeMinute = pch;

        int scheduleHour = atoi(scheduleExecuteTimeHour);
        int scheduleMinute = atoi(scheduleExecuteTimeMinute);

        pch = strtok (NULL, ",");

        if (scheduleHour == currentHour && scheduleMinute == currentMinute){
            char requestEndpoint[55];
            strcpy(requestEndpoint, "/LightDroidService/ExecuteSchedule?scheduleId=");
            strcat(requestEndpoint, pch);

            request.path = requestEndpoint;
            http.get(request, response, headers);
            Particle.publish("Application>\tExecute Schedule.");
        }
        pch = strtok (NULL, ",");
    }


    request.path = "/LightDroidService/GetSchedulesForUserWithPhotonAndPhillipsHue?userId=1";

    // Get request
    http.get(request, response, headers);

    url = response.body;

    digitalWrite(led1, HIGH);

    cleanedUpJson = url.replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"ExecuteTime\"", "").replace("\"ExternalId\"", "").replace("\"Action\"", "").replace(":", ",").replace(",,", ",").replace("\"", "");

    cleanJsonChar[cleanedUpJson.length() + 1];
    strcpy(cleanJsonChar, cleanedUpJson);

    pch = strtok (cleanJsonChar,",");

    while (pch != NULL)
    {
        char* scheduleAction = pch;
        pch = strtok (NULL, ",");
        char* scheduleExecuteTimeHour = pch;
        pch = strtok (NULL, ",");
        char* scheduleExecuteTimeMinute = pch;

        int scheduleHour = atoi(scheduleExecuteTimeHour);
        int scheduleMinute = atoi(scheduleExecuteTimeMinute);
        int action = atoi(scheduleAction);

        pch = strtok (NULL, ",");

        if (scheduleHour == currentHour && scheduleMinute == currentMinute){
            char requestEndpoint[60];
            request.hostname = "10.10.10.125";
            strcpy(requestEndpoint, "/api/33a814b5193c1b66cb313c727240b10/lights/");
            strcat(requestEndpoint, pch);
            strcat(requestEndpoint, "/state");

            request.path = requestEndpoint;

            String lightAction = "{\"on\": false}";
            if(action == 1){
                lightAction = "{\"on\": true}";
            }
            request.body = lightAction;

            http.put(request, response, headers);
            Particle.publish("Application>\tExecute Schedule.");
        }
        pch = strtok (NULL, ",");
    }

    nextTime = millis() + 15000;
    digitalWrite(led1, LOW);
}
